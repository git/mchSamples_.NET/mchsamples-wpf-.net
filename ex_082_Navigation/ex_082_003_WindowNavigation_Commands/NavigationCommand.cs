﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace ex_082_003_WindowNavigation_Commands
{
    public class NavigateCommand : ICommand, INotifyPropertyChanged
    {
        public event EventHandler CanExecuteChanged;

        public event PropertyChangedEventHandler PropertyChanged;

        public bool CanExecute(object parameter)
        {
            DataTemplate control = parameter as DataTemplate;
            if (control == null) return false;
            return true;
        }

        public void Execute(object parameter)
        {
            DataTemplate control = parameter as DataTemplate;
            if (control == null) throw new ArgumentException("parameter is not of type DataTemplate");

            SelectedTemplate = control;
        }

        void OnPropertyChanged([CallerMemberName] string propertyName = null)
            => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));

        public DataTemplate SelectedTemplate
        {
            get { return selectedTemplate; }
            set { selectedTemplate = value; OnPropertyChanged(); }
        }
        private DataTemplate selectedTemplate;

    }
}
