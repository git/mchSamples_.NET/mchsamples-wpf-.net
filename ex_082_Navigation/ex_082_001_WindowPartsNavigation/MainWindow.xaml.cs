﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ex_082_001_WindowPartsNavigation
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        static Dictionary<string, Func<UserControl>> factory = new Dictionary<string, Func<UserControl>>()
        {
            ["Content 1"] = () => new UserControl1(),
            ["Content 2"] = () => new UserControl2(),
        };

        private void ListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var content = (e.AddedItems[0] as ListBoxItem).Content as string;
            if(factory.ContainsKey(content))
            {
                mainPart.Content = factory[content]();
            }
        }
    }
}
