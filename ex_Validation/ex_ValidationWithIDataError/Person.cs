﻿using System.ComponentModel;

namespace ex_ValidationWithIDataError
{

    /// <summary>
    /// On implémente l'interface IDataErrorInfo pour ajouter le comportement de validation
    /// </summary>
    public class Person : IDataErrorInfo
    {
        public string Name { get; set; }

        public string Surname { get; set; }

        public int? Age { get; set; }



        /// <summary>
        /// Méthode qui permet de valider nos propriétés 
        /// </summary>
        /// <param name="columnName"></param>
        /// <returns></returns>
        public string this[string columnName]
        {
            get
            {

                switch (columnName)
                {
                    case nameof(Surname):
                        if (Surname != null && Surname.Length > 5)
                        {
                            return "Surname invalid : max 5 characters";
                        }
                        break;
                    case nameof(Age):
                        if (!Age.HasValue)
                        {
                            return "Age is required";
                        }
                        break;
                }

                return string.Empty;
            }
        }

        public string Error { get; }
    }
}
