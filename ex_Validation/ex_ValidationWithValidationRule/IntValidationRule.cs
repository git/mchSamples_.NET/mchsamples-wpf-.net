﻿using System.Globalization;
using System.Windows.Controls;

namespace ex_ValidationWithValidationRule
{

    /// <summary>
    /// Règle de validation d'un int 
    /// </summary>
    public class IntValidationRule : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            if (value == null)
            {
                return new ValidationResult(false, "Please enter a valid Int");
            }
            return new ValidationResult(true,null);
        }
    }
}
