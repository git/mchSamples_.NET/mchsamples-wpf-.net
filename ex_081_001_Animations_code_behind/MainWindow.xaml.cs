﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ex_081_001_Animations_code_behind
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void mButton_Click_1(object sender, RoutedEventArgs e)
        {
            DoubleAnimation anim = new DoubleAnimation();
            anim.From = 100;
            anim.To = 150;
            anim.AutoReverse = true;
            anim.AccelerationRatio = 0.33;
            anim.DecelerationRatio = 0.33;
            anim.Duration = TimeSpan.FromSeconds(0.5);
            mButton.BeginAnimation(Button.WidthProperty, anim);

            DoubleAnimation anim2 = new DoubleAnimation();
            anim2.From = 10;
            anim2.To = 18;
            anim2.AutoReverse = true;
            anim2.AccelerationRatio = 0.33;
            anim2.DecelerationRatio = 0.33;
            anim2.Duration = TimeSpan.FromSeconds(0.5);
            mTextBlock.BeginAnimation(TextBlock.FontSizeProperty, anim2);

            ColorAnimation anim3 = new ColorAnimation();
            anim3.From = Colors.Red;
            anim3.To = Colors.Yellow;
            anim3.AutoReverse = true;
            anim3.AccelerationRatio = 0.33;
            anim3.DecelerationRatio = 0.33;
            anim3.Duration = TimeSpan.FromSeconds(0.5);
            (Resources["mBrush"] as SolidColorBrush).BeginAnimation(SolidColorBrush.ColorProperty, anim3);

            DoubleAnimation anim4 = new DoubleAnimation();
            anim4.From = 1;
            anim4.To = -1;
            anim4.AutoReverse = true;
            anim4.AccelerationRatio = 0.33;
            anim4.DecelerationRatio = 0.33;
            anim4.Duration = TimeSpan.FromSeconds(0.25);
            anim4.RepeatBehavior = new RepeatBehavior(2);
            (mTextBlock.LayoutTransform as ScaleTransform).BeginAnimation(ScaleTransform.ScaleXProperty, anim4);
        }
    }
}
