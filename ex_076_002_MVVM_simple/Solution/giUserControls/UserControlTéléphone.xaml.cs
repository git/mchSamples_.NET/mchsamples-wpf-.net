﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace giUserControls
{
    /// <summary>
    /// Logique d'interaction pour UserControlTéléphone.xaml
    /// </summary>
    public partial class UserControlTéléphone : UserControl
    {
        public UserControlTéléphone()
        {
            InitializeComponent();
        }

        public static readonly DependencyProperty TypeProperty;

        public static readonly DependencyProperty NuméroProperty;

        static UserControlTéléphone()
        {
            UserControlTéléphone.TypeProperty = 
                DependencyProperty.Register("Type",
                                            typeof(string), 
                                            typeof(UserControlTéléphone), 
                                            new PropertyMetadata("Travail"));

            UserControlTéléphone.NuméroProperty =
                DependencyProperty.Register("Numéro",
                                            typeof(string),
                                            typeof(UserControlTéléphone),
                                            new PropertyMetadata("0123456789"));
        }

        public string Type
        {
            get
            {
                return GetValue(UserControlTéléphone.TypeProperty) as string;
            }
            set
            {
                SetValue(UserControlTéléphone.TypeProperty, value);
            }
        }

        public string Numéro
        {
            get
            {
                return GetValue(UserControlTéléphone.NuméroProperty) as string;
            }
            set
            {
                SetValue(UserControlTéléphone.NuméroProperty, value);
            }
        }

        //private void OnTéléphoneClicked(object sender, MouseButtonEventArgs e)
        //{
        //    MessageBox.Show("Appel en cours...");
        //}
    }
}
