﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using giContacts;
using System.Windows.Input;
using System.ComponentModel;

namespace ContactsWPF.ViewModels
{
    public class PersonneVM : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(String info)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(info));
            }
        }

        public Personne Model
        {
            get
            {
                return mModel;
            }
            set
            {
                mModel = value;
            }
        }
        private Personne mModel;

        public string Nom
        {
            get
            {
                return Model.Nom;
            }
            set
            {
                Model.Nom = value;
                OnPropertyChanged("Self");
            }
        }

        public string Prénom
        {
            get
            {
                return Model.Prénom;
            }
            set
            {
                Model.Prénom = value;
                OnPropertyChanged("Self");
            }
        }

        public string ImageSource
        {
            get
            {
                return Model.ImageSource;
            }
            set
            {
                Model.ImageSource = value;
                OnPropertyChanged("Self");
            }
        }

        public PersonneVM Self
        {
            get
            {
                return this;
            }
        }

        public Téléphone SelectedTéléphone
        {
            get
            {
                return mSelectedTéléphone;
            }
            set
            {
                mSelectedTéléphone = value;
            }
        }
        private Téléphone mSelectedTéléphone;

        public ICommand PhoneCalledCommand
        {
            get
            {
                return mPhoneCalledCommand;
            }
        }
        private ICommand mPhoneCalledCommand;

        public ICommand AddPhoneCommand
        {
            get
            {
                return mAddPhoneCommand;
            }
        }
        private ICommand mAddPhoneCommand;

        public ICommand RemovePhoneCommand
        {
            get
            {
                return mRemovePhoneCommand;
            }
        }
        private ICommand mRemovePhoneCommand;

        public PersonneVM()
        {
            mPhoneCalledCommand = new Commands.PhoneCallCommand(this);
            mAddPhoneCommand = new Commands.AddPhoneCommand(this);
            mRemovePhoneCommand = new Commands.RemovePhoneCommand(this);
        }

        public void RemovePhone(Téléphone phone)
        {
            Model.Téléphones.Remove(phone);
        }

        public void AddPhone(Téléphone phone)
        {
            Model.Téléphones.Add(phone);
        }
    }
}
