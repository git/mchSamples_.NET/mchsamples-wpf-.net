﻿using giContacts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContactsWPF.ViewModels
{
    public class WindowNewPhoneVM
    {
        public Téléphone Téléphone
        {
            get
            {
                return mTéléphone;
            }
            set
            {
                mTéléphone = value;
            }
        }
        private Téléphone mTéléphone = new Téléphone();

        public string Title
        {
            get;
            set;
        }

        public string Caption
        {
            get
            {
                return Title.Split(' ')[0];
            }
        }
    }
}
