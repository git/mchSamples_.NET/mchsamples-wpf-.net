﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace ex_074_011_MultiBinding_Converter
{
    class Nounours2AnonymousConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            string nom = values[0] as string;
            DateTime naissance = (DateTime)values[1];
            string nbPoils = values[2] as string;
            return new { Name = nom, BirthDate = naissance, Fur = nbPoils }.ToString();
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
