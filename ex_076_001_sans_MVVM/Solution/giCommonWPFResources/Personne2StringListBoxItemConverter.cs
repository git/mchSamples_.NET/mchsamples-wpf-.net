﻿using giContacts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace giCommonWPFResources
{
    [System.Windows.Data.ValueConversion(typeof(Personne), typeof(String))]
    public class Personne2StringListBoxItemConverter: IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var personne = value as Personne;
            if (personne == null)
            {
                return null;
            }

            return string.Format("{0} {1}", personne.Prénom, personne.Nom.ToUpper());
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
