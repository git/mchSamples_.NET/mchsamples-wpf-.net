﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace giContacts
{
    public enum TéléphoneType
    {
        Inconnu,
        Mobile,
        Domicile,
        Travail,
        Fax
    }
    
    /// <summary>
    /// un numéro de téléphone d'une personne
    /// </summary>
    public class Téléphone
    {
        /// <summary>
        /// le type de ce numéro de téléphone
        /// </summary>
        public TéléphoneType Type
        {
            get;
            set;
        }

        /// <summary>
        /// le numéro de téléphone
        /// </summary>
        public string Numéro
        {
            get;
            set;
        }
    }
}
