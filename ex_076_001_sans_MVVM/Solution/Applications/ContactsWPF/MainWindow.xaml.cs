﻿using giContacts;
using giUserControls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ContactsWPF
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            //var view = (Resources["listContactsView"] as CollectionViewSource).View;
            //view.SortDescriptions.Add(new SortDescription("Nom", ListSortDirection.Ascending));
        }

        private void OnAjouterTéléphoneUp(object sender, MouseButtonEventArgs e)
        {
            WindowNewPhone windowNewPhone = new WindowNewPhone();
            windowNewPhone.Closing += windowNewPhone_Closing;
            windowNewPhone.ShowDialog();
        }

        void windowNewPhone_Closing(object sender, CancelEventArgs e)
        {
            var window = sender as WindowNewPhone;

            TéléphoneType type;
            if(!Enum.TryParse(window.mComboBoxTypes.SelectedValue as string, out type))
            {
                type = TéléphoneType.Inconnu;
            }

            giContacts.Téléphone tel = new Téléphone()
            {
                Type = type,
                Numéro = window.mTextBoxNuméro.Text
            };
            
            (mListBoxChooseContact.SelectedItem as Personne).Téléphones.Add(tel);
        }

        private void RetirerNumero(object sender, RoutedEventArgs e)
        {
            ListView listViewTéléphones = (((sender as MenuItem).Parent as ContextMenu).PlacementTarget as ListView);
            Personne contact = (mListBoxChooseContact.SelectedItem as Personne);
            var selectedTel = (listViewTéléphones.SelectedItem as Téléphone);

            contact.Téléphones.Remove(contact.Téléphones.Single(tel => tel.Type == selectedTel.Type && tel.Numéro == selectedTel.Numéro));
        }

        private void OnTéléphoneClicked(object sender, MouseButtonEventArgs e)
        {
            if (e.Source is UserControlTéléphone)
            {
                MessageBox.Show(string.Format("Appel {0} {1} ({2}) en cours...",
                                                (mListBoxChooseContact.SelectedItem as Personne).Prénom,
                                                (mListBoxChooseContact.SelectedItem as Personne).Nom,
                                                (e.Source as UserControlTéléphone).Numéro));
            }
        }
    }
}
