﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ex_DataTemplateSelector
{
    class Rectangle : Forme
    {
        public double Width
        {
            get { return width; }
            set { width = value; OnPropertyChanged(); }
        }
        private double width;

        public double Height
        {
            get { return height; }
            set { height = value; OnPropertyChanged(); }
        }
        private double height;

        public override double X { get => base.X -Width*0.5; set => base.X = value; }

        public override double Y { get => base.Y - Height*0.5; set => base.Y = value; }
    }
}
