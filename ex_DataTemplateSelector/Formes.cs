﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ex_DataTemplateSelector
{
    class Formes : ObservableCollection<Forme>
    {
        public Formes()
        {
            Add(new Circle { X = 0, Y = 0, Diameter = 20 });
            Add(new Circle { X = 200, Y = 300, Diameter = 30 });
            Add(new Circle { X = 300, Y = 400, Diameter = 40 });
            Add(new Rectangle { X = 1, Y = 1, Width=20, Height=30 });
            Add(new Rectangle { X = 400, Y = 300, Width=20, Height= 50 });
        }
    }
}
