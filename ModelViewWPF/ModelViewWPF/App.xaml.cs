﻿using Model;
using Stub;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using XmlData;

namespace ModelViewWPF
{
    /// <summary>
    /// Logique d'interaction pour App.xaml
    /// </summary>
    public partial class App : Application
    {
        internal Manager Manager { get; } = new Manager(new XmlDataManager());

        //public App()
        //{
        //    Manager.DataManager = new XmlDataManager();
        //    foreach(var n in Manager.LesNounours)
        //    {
        //        Manager.DataManager.Add(n);
        //    }
        //}

    }
}
