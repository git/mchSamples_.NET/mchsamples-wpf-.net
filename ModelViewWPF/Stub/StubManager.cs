﻿using Model;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stub
{
    public class StubManager : IDataManager
    {
        public IEnumerable<Nounours> LesNounours
        {
            get
            {
                return new Nounours[]
                {
                    new Nounours {Id = 1, Nom = "Chewbacca", NbPoils=99999999,  Naissance=new DateTime(1977, 5, 27)},
                    new Nounours {Id = 2, Nom = "Chucky", NbPoils=1,  Naissance=new DateTime(1986, 9, 11)},
                    new Nounours {Id = 3, Nom = "Gizmo", NbPoils=123456789,  Naissance=new DateTime(1984, 6, 8)},
                };
            }
        }

        public void Add(Nounours nounours)
        {
            Debug.WriteLine($"Le nounours {nounours.Nom} a été ajouté");
        }

        public void Remove(Nounours nounours)
        {
            Debug.WriteLine($"Le nounours {nounours.Nom} a été enlevé");
        }

        public void Update(Nounours nounours)
        {
            Debug.WriteLine($"Le nounours {nounours.Nom} a été mis à jour");
        }
    }
}
