﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public interface IDataManager
    {
        IEnumerable<Nounours> LesNounours { get; }

        void Add(Nounours nounours);

        void Remove(Nounours nounours);

        void Update(Nounours nounours);
    }
}
