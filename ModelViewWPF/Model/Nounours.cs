﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    [DataContract]
    public class Nounours : IEquatable<Nounours>, INotifyPropertyChanged
    {
        private string  nom;

        [DataMember]
        public string  Nom
        {
            get { return nom; }
            set { nom = value; OnPropertyChanged(nameof(Nom)); }
        }


        private int nbPoils;

        [DataMember]
        public int NbPoils
        {
            get { return nbPoils; }
            set { nbPoils = value; OnPropertyChanged(nameof(NbPoils)); }
        }


        private DateTime naissance;

        [DataMember]
        public DateTime Naissance
        {
            get { return naissance; }
            set { naissance = value; OnPropertyChanged(nameof(Naissance)); }
        }

        private int id;

        [DataMember]
        public int Id
        {
            get { return id; }
            set { id = value; OnPropertyChanged(nameof(Id)); }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        void OnPropertyChanged(string propName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propName));
        }

        public override int GetHashCode()
        {
            return Id % 31;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(obj, null)) return false;
            if (ReferenceEquals(obj, this)) return true;
            if (GetType() != obj.GetType()) return false;
            return Equals(obj as Nounours);
        }

        public bool Equals(Nounours other)
        {
            return Id == other.Id;
        }

        public override string ToString()
        {
            return $"{Nom} ({NbPoils} poil(s), {Naissance.Year})";
        }
    }
}
