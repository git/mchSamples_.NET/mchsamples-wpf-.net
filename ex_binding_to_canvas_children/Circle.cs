﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace ex_binding_to_canvas_children
{
    public class Circle : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propName));
            }
        }

        public double Top
        {
            get
            {
                return mTop;
            }
            set
            {
                mTop = value;
                OnPropertyChanged("Top");
            }
        }
        private double mTop;

        public double Left
        {
            get;
            set;
        }

        public double Diameter
        {
            get
            {
                return mDiameter;
            }
            set
            {
                mDiameter = value;
                OnPropertyChanged("Diameter");
            }
        }
        private double mDiameter;

        public SolidColorBrush Color
        {
            get;
            set;
        }
    }
}
