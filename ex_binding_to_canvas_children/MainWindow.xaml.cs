﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ex_binding_to_canvas_children
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        

        private void RepeatButton_Click_1(object sender, RoutedEventArgs e)
        {
            (Resources["circles"] as Circles).Last().Top += 10;
        }

        private void ItemsControl_MouseRightButtonDown_1(object sender, MouseButtonEventArgs e)
        {
            (Resources["circles"] as Circles).Clear();
        }

        private void ItemsControl_MouseLeftButtonDown_1(object sender, MouseButtonEventArgs e)
        {
            (Resources["circles"] as Circles).Add(new Circle
            {
                Top = e.GetPosition(sender as ItemsControl).Y,
                Left = e.GetPosition(sender as ItemsControl).X,
                Diameter = 25,
                Color = Brushes.Red
            });
        }

        private void ItemsControl_MouseWheel_1(object sender, MouseWheelEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                (Resources["circles"] as Circles).Last().Diameter += e.Delta;
            }
        }
    }
}
