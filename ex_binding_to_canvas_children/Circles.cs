﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace ex_binding_to_canvas_children
{
    public class Circles : ObservableCollection<Circle>
    {
        public Circles()
        {
            Add(new Circle { Top = 10, Left = 15, Diameter = 10, Color = Brushes.DarkSeaGreen });
            Add(new Circle { Top = 20, Left = 30, Diameter = 15, Color = Brushes.BlueViolet });
            Add(new Circle { Top = 30, Left = 45, Diameter = 25, Color = Brushes.Crimson });
            Add(new Circle { Top = 50, Left = 70, Diameter = 35, Color = Brushes.DarkGoldenrod });
            Add(new Circle { Top = 70, Left = 90, Diameter = 25, Color = Brushes.Crimson });
        }

        protected override void InsertItem(int index, Circle item)
        {
            base.InsertItem(index, item);
            item.PropertyChanged += circle_PropertyChanged;
            OnPropertyChanged(new PropertyChangedEventArgs("Height"));
        }

        protected override void RemoveItem(int index)
        {
            base.RemoveItem(index);
            if (this[index] != null)
                this[index].PropertyChanged -= circle_PropertyChanged;
            OnPropertyChanged(new PropertyChangedEventArgs("Height"));
        }

        void circle_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            OnPropertyChanged(new PropertyChangedEventArgs("Height"));
        }

        public double Height
        {
            get
            {
                return this.Max(circle => circle.Top + circle.Diameter);
            }
        }
    }
}
