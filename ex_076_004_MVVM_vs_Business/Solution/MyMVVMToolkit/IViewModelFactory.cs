﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyMVVMToolkit
{
    public interface IViewModelFactory<TSource, TViewModel>
        where TViewModel : BaseViewModel<TSource>
    {
        TViewModel CreateVM(TSource source);
    }

    public class ViewModelFactory<TSource, TViewModel> : IViewModelFactory<TSource, TViewModel>
        where TViewModel: BaseViewModel<TSource>, new()
    {
        public TViewModel CreateVM(TSource source)
        {
            return new TViewModel() { Model = source };
        }
    }

}
