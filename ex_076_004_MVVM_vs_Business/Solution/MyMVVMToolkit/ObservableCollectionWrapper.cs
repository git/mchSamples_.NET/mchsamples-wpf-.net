﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyMVVMToolkit
{
    public class ObservableCollectionWrapper<TSource, TViewModel> : ICollection<TViewModel>, IDisposable, INotifyCollectionChanged
        where TViewModel : BaseViewModel<TSource>, new()
    {
        #region fields
        private ICollection<TSource> mWrappedCollection;
        private static IViewModelFactory<TSource, TViewModel> mViewModelFactory;
        #endregion

        public ObservableCollectionWrapper(ICollection<TSource> wrappedCollection, IViewModelFactory<TSource, TViewModel> viewModelFactory)
        {
            if (wrappedCollection == null)
            {
                throw new ArgumentNullException("wrappedCollection", "wrappedCollection must not be null.");
            }
            mWrappedCollection = wrappedCollection;
            mViewModelFactory = viewModelFactory;
        }

        public ObservableCollectionWrapper(ICollection<TSource> wrappedCollection)
            : this(wrappedCollection, new ViewModelFactory<TSource, TViewModel>())
        {
        }

        #region ICollection<TSource> Members
        public void Add(TViewModel item)
        {
            mWrappedCollection.Add((TSource)item.Model);
            OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, item));
        }

        public void Clear()
        {
            OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
            mWrappedCollection.Clear();
        }

        public bool Contains(TViewModel item)
        {
            return mWrappedCollection.Contains((TSource)item.Model);
        }

        public void CopyTo(TViewModel[] array, int arrayIndex)
        {
            mWrappedCollection.Select(item => mViewModelFactory.CreateVM(item)).ToArray().CopyTo(array, arrayIndex);
        }

        public int Count
        {
            get { return mWrappedCollection.Count; }
        }

        public bool IsReadOnly
        {
            get { return mWrappedCollection.IsReadOnly; }
        }

        public bool Remove(TViewModel item)
        {
            int index = mWrappedCollection.ToList().IndexOf((TSource)item.Model);
            if (mWrappedCollection.Remove((TSource)item.Model))
            {
                OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Remove, item, index));
                return true;
            }
            return false;
        }

        #endregion

        public class MyEnumerator : IEnumerator<TViewModel>
        {
            IEnumerator<TSource> mItor;
            internal MyEnumerator(IEnumerator<TSource> itor)
            {
                mItor = itor;
            }

            public TViewModel Current
            {
                get { return mViewModelFactory.CreateVM(mItor.Current); }
            }

            public void Dispose()
            {
                mItor.Dispose();
            }

            object IEnumerator.Current
            {
                get { return mViewModelFactory.CreateVM(mItor.Current); }
            }

            public bool MoveNext()
            {
                return mItor.MoveNext();
            }

            public void Reset()
            {
                mItor.Reset();
            }
        }

        #region IEnumerable<TViewModel> Members
        public IEnumerator<TViewModel> GetEnumerator()
        {
            return new MyEnumerator(mWrappedCollection.GetEnumerator());
        }
        #endregion

        #region IEnumerable Members
        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return new MyEnumerator(mWrappedCollection.GetEnumerator());
        }
        #endregion

        #region INotifyCollectionChanged Members
        public event NotifyCollectionChangedEventHandler CollectionChanged;
        private void OnCollectionChanged(NotifyCollectionChangedEventArgs eventArgs)
        {
            if (CollectionChanged != null)
            {
                CollectionChanged(this, eventArgs);
            }
        }
        #endregion

        #region IDisposable Members
        public void Dispose() { mWrappedCollection = null; }
        #endregion
    }
}
