﻿using giContacts;
using MyMVVMToolkit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContactsWPF.ViewModels
{
    public class AdresseVM : BaseViewModel<Adresse>
    {
        public int Numéro
        {
            get
            {
                return Model.Numéro;
            }
            set
            {
                Model.Numéro = value;
                OnPropertyChanged(() => Numéro);
            }
        }

        public string Rue
        {
            get
            {
                return Model.Rue;
            }
            set
            {
                Model.Rue = value;
                OnPropertyChanged(() => Rue);
            }
        }

        public string Ville
        {
            get
            {
                return Model.Ville;
            }
            set
            {
                Model.Ville = value;
                OnPropertyChanged(() => Ville);
            }
        }

        public int CodePostal
        {
            get
            {
                return Model.CodePostal;
            }
            set
            {
                Model.CodePostal = value;
                OnPropertyChanged(() => CodePostal);
            }
        }

        public static IEnumerable<string> AdresseTypes
        {
            get
            {
                return Enum.GetNames(typeof(Adresse.AdresseType));
            }
        }

        public string Type
        {
            get
            {
                return Model.Type.ToString();
            }
            set
            {
                Adresse.AdresseType result;
                if (!Enum.TryParse(value, out result))
                {
                    return;
                }
                Model.Type = result;
                OnPropertyChanged(() => Type);
            }
        }
    }
}
