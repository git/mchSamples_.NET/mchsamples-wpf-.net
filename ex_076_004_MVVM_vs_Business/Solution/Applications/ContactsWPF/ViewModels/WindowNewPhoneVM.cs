﻿using ContactsWPF.Views;
using giContacts;
using MyMVVMToolkit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace ContactsWPF.ViewModels
{
    public class WindowNewPhoneVM : BaseViewModel
    {
        public enum AddOrModify : byte
        {
            Add = 0,
            Modify = 1
        }

        public WindowNewPhoneVM(PersonneVM personne, AddOrModify type, WindowNewPhone window)
        {
            Window = window;
            Personne = personne;
            Type = type;
            switch (type)
            {
                default:
                case AddOrModify.Add:
                    Title = "Ajouter un nouveau numéro de téléphone";
                    Téléphone = new TéléphoneVM() { Model = new Téléphone() };
                    break;
                case AddOrModify.Modify:
                    Title = "Modifier un numéro de téléphone";
                    Téléphone = Personne.SelectedTéléphone;
                    break;
            }
        }

        public WindowNewPhone Window
        {
            get;
            set;
        }

        public AddOrModify Type
        {
            get;
            set;
        }

        public TéléphoneVM Téléphone
        {
            get;
            set;
        }

        public PersonneVM Personne
        {
            get;
            set;
        }

        public string Title
        {
            get;
            set;
        }

        public string Caption
        {
            get
            {
                return Title.Split(' ')[0];
            }
        }

        public ICommand CloseCommand
        {
            get
            {
                return mCloseCommand ?? (mCloseCommand = new RelayCommand(
                    () =>
                    {
                        switch (Type)
                        {
                            case AddOrModify.Add:
                                Personne.AddPhone(Téléphone);
                                break;
                            case AddOrModify.Modify:
                                Personne.SelectedTéléphone = Téléphone;
                                break;
                        }
                        Window.Close();
                    }));
            }
        }
        private ICommand mCloseCommand;

        public ICommand CancelCommand
        {
            get
            {
                return mCancelCommand ?? (mCancelCommand = new RelayCommand(
                    () =>
                    {
                        Window.Close();   
                    }));
            }
        }
        private ICommand mCancelCommand;
    }
}
