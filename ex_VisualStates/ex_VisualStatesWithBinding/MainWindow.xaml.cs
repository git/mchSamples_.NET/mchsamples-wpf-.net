﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
//using ex_VisualStatesWithBinding.Annotations;

namespace ex_VisualStatesWithBinding
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window,INotifyPropertyChanged
    {
        private RecState _recState;

        public RecState RecState
        {
            get { return _recState; }
            set
            {
                if (_recState != value)
                {
                    _recState = value;
                    OnPropertyChanged();
                }
            }
        }

        public MainWindow()
        {
            InitializeComponent();
            DataContext = this;

        }

        private void OnGoToSmall(object sender, RoutedEventArgs e)
        {
            //Changement d'état, l'état "Small" est appliqué sur la vue
            RecState = RecState.Small;
        }

        private void OnGoToLarge(object sender, RoutedEventArgs e)
        {
            //Changement d'état, l'état "Large" est appliqué sur la vue
            RecState = RecState.Large;
        }

        private void OnGoToVisible(object sender, RoutedEventArgs e)
        {
            //Changement d'état, l'état "Visible" est appliqué sur la vue
            RecState = RecState.Visible;
        }

        private void OnGoToCollapsed(object sender, RoutedEventArgs e)
        {
            //Changement d'état, l'état "Collapsed" est appliqué sur la vue
            RecState = RecState.Collapsed;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        //[NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }

    public enum RecState
    {
        Small,
        Large,
        Visible,
        Collapsed
    }
}
