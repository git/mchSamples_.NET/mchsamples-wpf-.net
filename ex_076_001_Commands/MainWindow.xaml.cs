﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ex_076_001_Commands
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void OnOpen(object sender, ExecutedRoutedEventArgs e)
        {
            // Configure open file dialog box
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
            dlg.InitialDirectory = "C:\\Users\\Public\\Pictures\\Sample Pictures";
            dlg.FileName = "Images"; // Default file name
            dlg.DefaultExt = ".jpg | .png | .gif"; // Default file extension
            dlg.Filter = "All images files (.jpg, .png, .gif)|*.jpg;*.png;*.gif|JPG files (.jpg)|*.jpg|PNG files (.png)|*.png|GIF files (.gif)|*.gif"; // Filter files by extension 

            // Show open file dialog box
            bool? result = dlg.ShowDialog();

            // Process open file dialog box results 
            if (result == true)
            {
                // Open document 
                string filename = dlg.FileName;
                (Resources["mImageInfo"] as ImageInfo).ImageSource = filename;
            }
        }

        private void OnIsImage(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = (Resources["mImageInfo"] as ImageInfo).ImageSource != null;
        }

        private void OnClose(object sender, ExecutedRoutedEventArgs e)
        {
            (Resources["mImageInfo"] as ImageInfo).ImageSource = null;
        }

        private void OnZoomIn(object sender, ExecutedRoutedEventArgs e)
        {
            (Resources["mImageInfo"] as ImageInfo).Zoom *= 1.5;
        }

        private void OnZoomOut(object sender, ExecutedRoutedEventArgs e)
        {
            (Resources["mImageInfo"] as ImageInfo).Zoom /= 1.5;
        }

        private void OnScale1(object sender, ExecutedRoutedEventArgs e)
        {
            (Resources["mImageInfo"] as ImageInfo).Zoom = 1.0;
        }
    }
}
