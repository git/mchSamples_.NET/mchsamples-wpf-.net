﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Documents;
using System.Windows.Markup;

namespace ex_076_001_Commands
{
    [System.Windows.Data.ValueConversion(typeof(String), typeof(FlowDocument))]
    public class Path2FlowDocumentConverter : System.Windows.Data.IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            string filename = string.Format("{0}\\{1}", Directory.GetCurrentDirectory().ToString(), value as string);
            FileStream xamlFile = new FileStream(filename, FileMode.Open, FileAccess.Read);
            FlowDocument content = XamlReader.Load(xamlFile) as FlowDocument;
            return content;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
