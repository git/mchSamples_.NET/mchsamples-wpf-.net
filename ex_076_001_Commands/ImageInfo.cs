﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ex_076_001_Commands
{
    class ImageInfo : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(String info)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(info));
            }
        }

        public string ImageSource
        {
            get
            {
                return mImageSource;
            }
            internal set
            {
                mImageSource = value;
                OnPropertyChanged("ImageSource");
            }
        }
        private string mImageSource;

        public double Zoom
        {
            get
            {
                return mZoom;
            }
            internal set
            {
                mZoom = value;
                OnPropertyChanged("Zoom");
            }
        }
        private double mZoom = 300.0;
    }
}
