﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ex_074_023_DependencyProperty3
{
    public class Manager : INotifyPropertyChanged
    {
        public IEnumerable<Nounours> LesNounours => lesNounours;

        private List<Nounours> lesNounours = new List<Nounours>()
        {
            new Nounours {Nom = "Chucky", NbPoils=42, Naissance = new DateTime(1988, 6, 8)},
            new Nounours {Nom = "Gizmo", NbPoils=987654321, Naissance = new DateTime(1986, 11, 9)},
            new Nounours {Nom = "Chewbacca", NbPoils=123456789, Naissance = new DateTime(1977, 5, 27)},
        };

        private Nounours nounoursSélectionné;

        public Nounours NounoursSélectionné
        {
            get { return nounoursSélectionné; }
            set { nounoursSélectionné = value; OnPropertyChanged("NounoursSélectionné"); }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        void OnPropertyChanged(string propertyName)
            => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }
}
