﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ex_074_023_DependencyProperty3
{
    public class Nounours
    {
        public string Nom { get; set; }

        public int NbPoils { get; set; }

        public DateTime Naissance { get; set; }

        public string ImageName => $"{Nom.ToLower()}.jpg";
    }
}
