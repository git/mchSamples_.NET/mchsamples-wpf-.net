﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace ex_074_024_DependencyProperty3
{
    public class Nounours : INotifyPropertyChanged
    {
        private string nom;

        public event PropertyChangedEventHandler PropertyChanged;

        public string Nom
        {
            get { return nom; }
            set { nom = value; OnPropertyChanged(); }
        }

        void OnPropertyChanged([CallerMemberName] string propertyName = null)
            => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));

        private int nbPoils;

        public int NbPoils
        {
            get { return nbPoils; }
            set { nbPoils = value; OnPropertyChanged(); }
        }


        private DateTime naissance;

        public DateTime Naissance
        {
            get { return naissance; }
            set { naissance = value; OnPropertyChanged(); }
        }


        public string ImageName => $"{Nom.ToLower()}.jpg";
    }
}
