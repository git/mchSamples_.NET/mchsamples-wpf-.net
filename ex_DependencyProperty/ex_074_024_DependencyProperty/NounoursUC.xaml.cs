﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ex_074_024_DependencyProperty3
{
    /// <summary>
    /// Logique d'interaction pour NounoursUC.xaml
    /// </summary>
    public partial class NounoursUC : UserControl
    {
        public NounoursUC()
        {
            InitializeComponent();
        }



        public string NomNounours
        {
            get { return (string)GetValue(NomNounoursProperty); }
            set { SetValue(NomNounoursProperty, value); }
        }

        // Using a DependencyProperty as the backing store for NomNounours.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty NomNounoursProperty =
            DependencyProperty.Register("NomNounours", typeof(string), typeof(NounoursUC), new PropertyMetadata("anonymous"));



        public int NbPoils
        {
            get { return (int)GetValue(NbPoilsProperty); }
            set { SetValue(NbPoilsProperty, value); }
        }

        // Using a DependencyProperty as the backing store for NbPoils.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty NbPoilsProperty =
            DependencyProperty.Register("NbPoils", typeof(int), typeof(NounoursUC), new PropertyMetadata(0));




        public DateTime NounoursNaissance
        {
            get { return (DateTime)GetValue(NounoursNaissanceProperty); }
            set { SetValue(NounoursNaissanceProperty, value); }
        }

        // Using a DependencyProperty as the backing store for NounoursNaissance.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty NounoursNaissanceProperty =
            DependencyProperty.Register("NounoursNaissance", typeof(DateTime), typeof(NounoursUC), new PropertyMetadata(DateTime.Now));




        public string NounoursImage
        {
            get { return (string)GetValue(NounoursImageProperty); }
            set { SetValue(NounoursImageProperty, value); }
        }

        // Using a DependencyProperty as the backing store for NounoursImage.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty NounoursImageProperty =
            DependencyProperty.Register("NounoursImage", typeof(string), typeof(NounoursUC), new PropertyMetadata("babar.jpg"));

        public Manager LeManager => (Application.Current as App).LeManager;

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            LeManager.NounoursSélectionné = LeManager.LesNounours.First();
        }
    }
}
