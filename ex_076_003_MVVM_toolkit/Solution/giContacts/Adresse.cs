﻿using MyMVVMToolkit;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace giContacts
{
    /// <summary>
    /// une adresse d'une personne
    /// </summary>
    public class Adresse : ObservableObject
    {
        /// <summary>
        /// le numéro de la maison ou du bâtiment dans la rue
        /// </summary>
        public int Numéro
        {
            get
            {
                return mNuméro;
            }
            set
            {
                SetProperty(ref mNuméro, value, () => Numéro);
            }
        }
        private int mNuméro;

        /// <summary>
        /// le nom de la rue
        /// </summary>
        public string Rue
        {
            get
            {
                return mRue;
            }
            set
            {
                SetProperty(ref mRue, value, () => Rue);
            }
        }
        private string mRue;

        /// <summary>
        /// la ville
        /// </summary>
        public string Ville
        {
            get
            {
                return mVille;
            }
            set
            {
                SetProperty(ref mVille, value, () => Ville);
            }
        }
        private string mVille;

        /// <summary>
        /// le code postal
        /// </summary>
        public int CodePostal
        {
            get
            {
                return mCodePostal;
            }
            set
            {
                SetProperty(ref mCodePostal, value, () => CodePostal);
            }
        }
        /// <see cref="CodePostal"/>
        private int mCodePostal;

        /// <summary>
        /// les différents types d'Adresse possibles
        /// </summary>
        public enum AdresseType
        {
            Inconnu,
            Domicile,
            Travail
        }

        /// <summary>
        /// le type de cette adresse
        /// </summary>
        public AdresseType Type
        {
            get
            {
                return mType;
            }
            set
            {
                SetProperty(ref mType, value, () => Type);
            }
        }
        private AdresseType mType;
    }
}
