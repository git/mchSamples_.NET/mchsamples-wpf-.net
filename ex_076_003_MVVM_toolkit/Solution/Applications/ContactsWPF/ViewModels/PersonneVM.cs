﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using giContacts;
using System.Windows.Input;
using MyMVVMToolkit;
using System.Windows;
using System.ComponentModel;

namespace ContactsWPF.ViewModels
{
    public class PersonneVM : BaseViewModel<Personne>
    {
        public string Nom
        {
            get
            {
                return Model.Nom;
            }
            set
            {
                Model.Nom = value;
                OnPropertyChanged(() => Self);
            }
        }

        public string Prénom
        {
            get
            {
                return Model.Prénom;
            }
            set
            {
                Model.Prénom = value;
                OnPropertyChanged(() => Self);
            }
        }

        public string ImageSource
        {
            get
            {
                return Model.ImageSource;
            }
            set
            {
                Model.ImageSource = value;
                OnPropertyChanged(() => Self);
            }
        }

        public PersonneVM Self
        {
            get
            {
                return this;
            }
        }

        public Téléphone SelectedTéléphone
        {
            get
            {
                return mSelectedTéléphone;
            }
            set
            {
                mSelectedTéléphone = value;
            }
        }
        private Téléphone mSelectedTéléphone;

        public ICommand PhoneCalledCommand
        {
            get
            {
                return mPhoneCalledCommand ?? (mPhoneCalledCommand = new RelayCommand(
                    () =>
                    {
                        MessageBox.Show(string.Format("Appel {0} {1} ({2}) en cours...",
                                            this.Prénom,
                                            this.Nom,
                                            this.SelectedTéléphone.Numéro));
                    },
                    () =>
                    {
                        if (this.SelectedTéléphone == null)
                        {
                            return false;
                        }
                        return true;
                    }));
            }
        }
        private ICommand mPhoneCalledCommand;

        public ICommand AddPhoneCommand
        {
            get
            {
                return mAddPhoneCommand ?? (mAddPhoneCommand = new RelayCommand<string>(
                    param =>
                    {
                        string title;
                        Views.WindowNewPhone windowNewPhone = new Views.WindowNewPhone();
                        switch (param)
                        {
                            default:
                            case "Add":
                                title = "Ajouter un nouveau numéro de téléphone";
                                break;
                            case "Modify":
                                title = "Modifier un numéro de téléphone";
                                windowNewPhone.ViewModel.Téléphone = this.SelectedTéléphone;
                                break;
                        }
                        windowNewPhone.ViewModel.Title = title;
                        windowNewPhone.Closing += (sender, args) =>
                        {
                            if (args.Cancel)
                            {
                                return;
                            }
                            var window = sender as Views.WindowNewPhone;
                            this.AddPhone(window.ViewModel.Téléphone);
                        };
                        windowNewPhone.ShowDialog(); 
                    },
                    param =>
                    {
                        switch (param)
                        {
                            case "Add":
                                break;
                            case "Modify":
                                if (this.SelectedTéléphone == null)
                                {
                                    return false;
                                }
                                break;
                            default:
                                return false;
                        }
                        return true;
                    }));
            }
        }
        private ICommand mAddPhoneCommand;

        public ICommand RemovePhoneCommand
        {
            get
            {
                return mRemovePhoneCommand ?? (mRemovePhoneCommand = new RelayCommand(
                    () =>
                    {
                        this.RemovePhone(this.SelectedTéléphone);
                    },
                    () =>
                    {
                        if (this == null || this.SelectedTéléphone == null)
                        {
                            return false;
                        }
                        return true;
                    }));
            }
        }
        private ICommand mRemovePhoneCommand;

        public void RemovePhone(Téléphone phone)
        {
            Model.Téléphones.Remove(phone);
        }

        public void AddPhone(Téléphone phone)
        {
            Model.Téléphones.Add(phone);
        }
    }
}
